//
//  SelectBankViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/21/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class SelectBankViewController: UIViewController  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

extension SelectBankViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
}
