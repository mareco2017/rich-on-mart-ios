//
//  Color.swift
//  Rich on Mart
//
//  Created by Edison on 2/21/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc static let orangeBorder = UIColor(red: 255, green: 75, blue: 1)
    @nonobjc static let primary = UIColor(red: 0, green: 195, blue: 36)
    @nonobjc static let grayROM = UIColor(red: 142, green: 142, blue: 147)
    
    convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
        self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    static func returnRGBColor(r:CGFloat, g:CGFloat, b:CGFloat, alpha:CGFloat) -> UIColor{
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
    
}


