//
//  CartViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/28/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    @IBOutlet weak var notifLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notifLabel.layer.borderWidth = 1
        self.notifLabel.layer.borderColor = UIColor.primary.cgColor
    }
    
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartTableCell
        let rectShape = CAShapeLayer()
        rectShape.bounds = cell.saveLbl.frame
        rectShape.position = cell.saveLbl.center
        rectShape.path = UIBezierPath(roundedRect: cell.saveLbl.bounds, byRoundingCorners: [.topRight , .bottomRight], cornerRadii: CGSize(width: 8, height: 8)).cgPath
        
        //Here I'm masking the textView's layer with rectShape layer
        cell.saveLbl.layer.mask = rectShape
        return cell
    }
}

class CartTableCell: UITableViewCell {
    
    @IBOutlet weak var saveLbl: UILabel!
    
}
