//
//  ProductSearchViewController.swift
//  Rich on Mart
//
//  Created by Edison on 3/8/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class ProductSearchViewController: UIViewController {
    
    let populerFilter = ["Bayam", "Kangkung", "Apel", "Semangka", "Ayam Bakar", "Mie Goreng Seafood", "Soto"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for family in UIFont.familyNames as [String]
        {
            print("\(family)")
            
            for name in UIFont.fontNames(forFamilyName: family)
            {
                print("   \(name)")
            }
        }
    }
    
}

extension ProductSearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartTableCell
        let rectShape = CAShapeLayer()
        rectShape.bounds = cell.saveLbl.frame
        rectShape.position = cell.saveLbl.center
        rectShape.path = UIBezierPath(roundedRect: cell.saveLbl.bounds, byRoundingCorners: [.topRight , .bottomRight], cornerRadii: CGSize(width: 8, height: 8)).cgPath
        cell.saveLbl.layer.mask = rectShape
        return cell
    }
}

extension ProductSearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.populerFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = populerFilter[indexPath.row]
        let width = UILabel.textWidth(font: UIFont(name: "SFProText-Regular", size: 14)!, text: text)
        print(width)
        return CGSize(width: width + 40, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProductFilterCollectionViewCell
        cell.filterLbl.text = populerFilter[indexPath.row]
        return cell
    }
}

class ProductFilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterLbl: UILabel!
    
}
