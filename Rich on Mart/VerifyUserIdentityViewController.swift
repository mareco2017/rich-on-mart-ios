//
//  VerifyUserIdentityViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/26/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class VerifyUserIdentityViewController: UIViewController {
    
    @IBOutlet weak var verifyButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.verifyButton.frame
        rectShape.position = self.verifyButton.center
        rectShape.path = UIBezierPath(roundedRect: self.verifyButton.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 15, height: 15)).cgPath
        
        //Here I'm masking the textView's layer with rectShape layer
        self.verifyButton.layer.mask = rectShape
    }
    
}
