//
//  AddBankAccountViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/21/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class AddBankAccountViewController: UIViewController {
    
    @IBOutlet weak var noteView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteView.layer.borderWidth = 1
        noteView.layer.borderColor = UIColor.orangeBorder.cgColor
    }
    
}
