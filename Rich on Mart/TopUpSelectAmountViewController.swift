//
//  TopUpSelectAmountViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/22/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class TopUpSelectAmountViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension TopUpSelectAmountViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TopUpAmountCollectionViewCell
        
//        cell.titleLabel.text = "Rp \(amountList[indexPath.item].amount.formattedWithSeparator)"
        cell.titleLabel.textColor = UIColor.primary
        cell.roundedCornerView.backgroundColor = UIColor(hex: "E8E9EA")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if selectedAmountTag != nil {
//            let previousIndexPath = IndexPath(item: selectedAmountTag, section: 0)
//            let cell = collectionView.cellForItem(at: previousIndexPath) as! TopUpAmountCollectionViewCell
//            unhighlightPreviousCellView(cell: cell)
//        }
//        let cell = collectionView.cellForItem(at: indexPath) as! TopUpAmountCollectionViewCell
//        highlightSelectedCellView(cell: cell)
//        selectedAmountTag = indexPath.item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width / 3 - 8
        return CGSize(width: itemWidth , height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}

class TopUpAmountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var roundedCornerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
}

