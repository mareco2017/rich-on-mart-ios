//
//  HomeViewController.swift
//  Rich on Mart
//
//  Created by Edison on 1/30/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class LandingViewController: UITabBarController, UITabBarControllerDelegate {
    
    var perviousIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let tabViewController1 = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
        let tabViewController2 = self.storyboard!.instantiateViewController(withIdentifier: "TransactionViewController")
        let tabViewController3 = self.storyboard!.instantiateViewController(withIdentifier: "CartViewController")
        let tabViewController4 = self.storyboard!.instantiateViewController(withIdentifier: "AccountViewController")
        
        tabViewController1.tabBarItem = UITabBarItem(
            title: "Beranda",
            image:UIImage(named: "tab_home") ,
            tag:1)
        tabViewController2.tabBarItem = UITabBarItem(
            title: "Transaksi",
            image: UIImage(named: "tab_transaction"),
            tag: 2)
        
        tabViewController3.tabBarItem = UITabBarItem(
            title: "Troli",
            image:UIImage(named: "tab_cart") ,
            tag:3)
        tabViewController4.tabBarItem = UITabBarItem(
            title: "Akun",
            image:UIImage(named: "tab_account") ,
            tag:4)
        
        let navigationController1 = UINavigationController(rootViewController: tabViewController1)
        navigationController1.hidesBottomBarWhenPushed = true
        let navigationController2 = UINavigationController(rootViewController: tabViewController2)
        navigationController2.hidesBottomBarWhenPushed = true
        let navigationController3 = UINavigationController(rootViewController: tabViewController3)
        navigationController3.hidesBottomBarWhenPushed = true
        
        let controllers = [navigationController1, navigationController2, navigationController3, tabViewController4]
        
        self.viewControllers = controllers
        
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        self.perviousIndex = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //        if tabBarController.selectedIndex == 2{
        //            tabBarController.selectedIndex = self.perviousIndex
        //            self.showAlertProgress()
        //        }
    }
    
    
    
}
