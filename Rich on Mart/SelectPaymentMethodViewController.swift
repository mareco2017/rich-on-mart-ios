//
//  SelectPaymentMethodViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/27/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class SelectPaymentMethodViewController: UIViewController {
    
    var methodTitle = ["Bayar Ditempat (COD)", "Saldo", "Transfer Bank"]
    var methodDescription = ["Bayar saat menerima barang", "Bayar instan dengan saldo", "Transfer dengan rekening bank"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

extension SelectPaymentMethodViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return methodTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentMethodTableCell
        cell.descriptionLbl.text = methodDescription[indexPath.row]
        cell.titleLbl.text = methodTitle[indexPath.row]
        return cell
    }
}

class PaymentMethodTableCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
}
