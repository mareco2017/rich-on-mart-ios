//
//  HomeViewController.swift
//  Rich on Mart
//
//  Created by Edison on 2/1/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit
import ImageSlideshow

class HomeViewController: UIViewController {
    
    @IBOutlet weak var slideshow: ImageSlideshow!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slideshow.slideshowInterval = 5.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .customUnder(padding: 0))
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.primary
        pageControl.pageIndicatorTintColor = UIColor.grayROM
        slideshow.pageIndicator = pageControl
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            
        }
        slideshow.setImageInputs([
            ImageSource(image: UIImage(named: "apple")!), ImageSource(image: UIImage(named: "apple")!), ImageSource(image: UIImage(named: "apple")!)])
        
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2 {
            let padding: CGFloat =  40
            let collectionViewSize = collectionView.frame.size.width - padding
            return CGSize(width: collectionViewSize/2, height: 246)
        }
        else if collectionView.tag == 1 {
            return CGSize(width: 240, height: 104)
        }
        else {
            return CGSize(width: 134, height: 220)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        return cell
    }
    
    
}
